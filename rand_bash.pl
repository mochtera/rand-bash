package Rand_bash;
  use LWP::Simple;
  use utf8;

  my $url = "http://bash.org.pl/random/";
  my $content = get $url;
    die "Couldn't get $url" unless defined $content;
  
  my $bash = $content;
  $bash =~ s/\n//g;
  $bash =~ s/\R//g;
  $bash =~ s/\t//g;
  #print $bash."\n";
  $bash =~ m/post-body"\>(.*?)\<\/div/;
  $bash = $1;

  $bash =~ s/<br \/>/\n/g;
  $bash =~ s/<br>/\n/g;
  $bash =~ s/<\/br>/\n/g;
  $bash =~ s/&lt;/</g;
  $bash =~ s/&gt;/>/g;
  $bash =~ s/&quot;/"/g;
  print $bash."\n";
